# Back PHP

## Initialisation

Afin de pallier aux problèmes de compatibilité entre windows et linux, sans passer par la solution docker précédente. Nous avons mis en place deux fichier `index.php`.

- `index.php.linux`
- `index.php.windows`

Pour que le projet fonctionne, il faut copier l'un de ces fichiers selon le système d'exploitation en `index.php`.

## API

Les requêtes peuvent être effectuées via l'url suivante : <https://sellmeout.alexis-bonal.fr/api>

### Requêtes de connexion

| TODO | Méthode | Route   | Paramètres                          | Description                      |
| ---- | ------- | ------- | ----------------------------------- | -------------------------------- |
| ✅   | POST    | /signup | `mail`, `password`, `pseudo`,`role` | Crée un nouvel utilisateur       |
| ✅   | POST    | /signin | `mail`, `password`                  | Connecte un utilisateur existant |

### Actions sur les utilisateurs

| TODO | Méthode | Route           | Paramètres | Description                                |
| ---- | ------- | --------------- | ---------- | ------------------------------------------ |
| ✅   | GET     | /users/[userId] |            | Récupère les informations d'un utilisateur |

### Gestion des produits

| TODO | Méthode | Route                             | Paramètres                             | Description                                 |
| ---- | ------- | --------------------------------- | -------------------------------------- | ------------------------------------------- |
| ✅   | GET     | /products                         |                                        | Liste tous les produits existants           |
| ✅   | GET     | /products/search/[query]          |                                        | Recherche des produits par nom              |
| ✅   | GET     | /products/[userId]                |                                        | Liste uniquement les produits d'un marchand |
| ✅   | GET     | /products/[userId]/search/[query] |                                        | Recherche les produits d'un marchand        |
| ✅   | GET     | /product/[productId]              |                                        | Affiche les détails d'un produit            |
| ✅   | POST    | /product                          | `nom`, `description`, `prix`, `userId` | Création d'un produit                       |
| ✅   | PUT     | /product/[productId]/visibility   |                                        | Switch la visibilité d'un produit           |
| 🔲   | PUT     | /product/[productId]/stars        | `stars`                                | Modification de la note d'un produit        |

### Gestion des commandes

| TODO | Méthode | Route       | Paramètres | Description                             |
| ---- | ------- | ----------- | ---------- | --------------------------------------- |
| 🔲   | POST    | /order      | `userId`   | Nouvelle commande                       |
| 🔲   | GET     | /orders     | `userId`   | Récupère les commandes d'un utilisateur |
| 🔲   | GET     | /order/[id] |            | Récupération d'une commande             |
