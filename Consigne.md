# Challenge stack Php-html-css-js

## Introduction

La société SellMeOut, leader du destockage de matériel informatique dans plus de 120 magasins en
France souhaite lancer un nouveau canal de vente afin de conquérir un nouveau marché.

## Besoin

Celle-ci a décider de s'orienter sur un market place, qui permettra à la fois pour l'entreprise de
vendre ses propres produits mis en avant, mais aussi à d'autres entreprise de proposer leur propres
produits à la vente via une commission reversée à SellMeOut.
Dans ce but la plateforme doit être fonctionnelle dans sa version minimum dans un court délai afin
de lancer le marketing sur le produit à la rentrée prochaine.

## Mission

Vous avez obtenu le marché suite à la réponse à l'appel d'offre et devez maintenant proposer une
solution MVP dans les plus brefs délais.
Cette solution devra permettre à minima de :

| Utilisateur standard                                          | Route API                     | Description                                                                                                          |
| ------------------------------------------------------------- | ----------------------------- | -------------------------------------------------------------------------------------------------------------------- |
| S'inscrire/se connecter                                       | POST /users/signup            | Crée un nouvel utilisateur                                                                                           |
|                                                               | POST /users/login             | Connecte un utilisateur existant                                                                                     |
| Voir les produits en vente                                    | GET /products                 | Récupère tous les produits en vente                                                                                  |
| Rechercher des produits par leur nom                          | GET /products?search={nom}    | Recherche les produits par leur nom                                                                                  |
| Voir le détail d'un produit                                   | GET /products/{productId}     | Récupère les informations détaillées d'un produit spécifique                                                         |
| Ajouter un ou plusieurs produits au panier                    | POST /cart                    | Ajoute un produit ou plusieurs produits au panier de l'utilisateur (requête : { "productId": "123", "quantity": 2 }) |
| Valider une commande (sans module de paiement pour le moment) | POST /orders                  | Valide la commande avec les produits du panier                                                                       |
| Voir le récapitulatif des commandes                           | GET /orders                   | Récupère le récapitulatif des commandes de l'utilisateur                                                             |
| Ajouter une note à un produit commandé                        | POST /orders/{orderId}/rating | Ajoute une note à un produit commandé spécifique (requête : { "productId": "123", "rating": 4 })                     |

| Vendeur                         | Route API                            | Description                                                                                                                                     |
| ------------------------------- | ------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------- |
| S'inscrire/se connecter         | POST /sellers/signup                 | Crée un nouveau vendeur                                                                                                                         |
|                                 | POST /sellers/login                  | Connecte un vendeur existant                                                                                                                    |
| Gérer ses propres produits      | GET /sellers/products                | Récupère tous les produits du vendeur                                                                                                           |
| Ajouter un nouveau produit      | POST /sellers/products               | Ajoute un nouveau produit au catalogue du vendeur (requête : { "name": "Produit A", "description": "Description du produit A", "price": 9.99 }) |
| Supprimer un produit spécifique | DELETE /sellers/products/{productId} | Supprime un produit spécifique du catalogue du vendeur                                                                                          |
| Accéder aux commandes passées   | GET /sellers/orders                  | Récupère les commandes passées au vendeur                                                                                                       |

La note moyenne du vendeur est calculée par la moyenne de toutes les notes de leur produits
vendus.
L'entreprise SellMeOut aura un compote vendeur, avec la particularité que seules les notes produits
doivent apparaitre sur leurs produits, mais pas la note entreprise.
Si d'autres fonctionnalités vous paraissent nécessaires, vous êtes libres de les réaliser, mais pas au
détriment des fonctionnalités minimum demandées

## Stack technique

Pour réaliser cette solution vous utiliserez comme technologies :

- Php 8 (pas de framework type symfony, laravel...)
- HTML/CSS
- Javascript

Vous pouvez travailler soit via une construction de page en template en php, ou choisir de réaliser
une api REST et une application frontend séparée.
L'utilisation de framework front est possible (react, angular, vuejs), mais pas nécessaire.

## Contact

En cas de besoin, vous pouvez entrer en contact avec notre expert en interne Aurélien VAAST, qui sera à même de vous conseiller sur vos choix, et pourra libérer une heure de son temps pour vous aider en cas de difficulté.

## Livrables

Vous devrez fournir votre solution sur un repo git (github ou gitlab) avec des commits réguliers et pertinents.
