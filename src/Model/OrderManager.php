<?php

namespace Model;


class OrderManager extends ModelManager
{

    public function __construct()
    {
        parent::__construct("orders");
    }

    public function getUserOrders($userId)
    {
        $query = "SELECT o.orderId, o.date, o.prixTotal, o.userId, GROUP_CONCAT(CONCAT('{\"id\": \"', p.id, '\", \"nom\": \"', p.nom, '\", \"description\": \"', p.description, '\", \"image\": \"', p.image, '\", \"prix\": \"', p.prix, '\", \"userId\": \"', p.userId, '\", \"visibility\": \"', p.visibility, '\", \"quantity\": \"', op.quantity, '\", \"subtotal\": \"', (op.quantity * p.prix), '\", \"note\": \"', COALESCE(r.value, 0), '\"}') SEPARATOR ',') AS produits
          FROM orders AS o
          JOIN orders_product AS op ON o.orderId = op.orderId
          JOIN product AS p ON op.productId = p.id
          LEFT JOIN rating AS r ON p.id = r.productId AND r.userId = :userId
          WHERE o.userId = :userId
          GROUP BY o.orderId";
        $stmt = $this->bdd->prepare($query);
        $stmt->bindParam(':userId', $userId);
        $stmt->execute();
        $orders = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $formattedOrders = [];
        foreach ($orders as $order) {
            $produits = '[' . $order['produits'] . ']';
            $formattedOrder = [
                'idUtilisateur' => $order['userId'],
                'idCommande' => $order['orderId'],
                'dateCommande' => $order['date'],
                'prixTotalCommande' => $order['prixTotal'],
                'produits' => json_decode($produits, true)
            ];
            $formattedOrders[] = $formattedOrder;
        }

        return $formattedOrders;
    }

    public function getOrderById($orderId, $userId)
    {
        $query = "SELECT o.orderId, o.date, o.prixTotal, GROUP_CONCAT(CONCAT('{\"id\": \"', p.id, '\", \"nom\": \"', p.nom, '\", \"description\": \"', p.description, '\", \"image\": \"', p.image, '\", \"prix\": \"', p.prix, '\", \"userId\": \"', p.userId, '\", \"visibility\": \"', p.visibility, '\", \"quantity\": \"', op.quantity, '\", \"subtotal\": \"', (op.quantity * p.prix), '\", \"note\": \"', COALESCE(r.value, 0), '\"}') SEPARATOR ',') AS produits
              FROM orders AS o
              JOIN orders_product AS op ON o.orderId = op.orderId
              JOIN product AS p ON op.productId = p.id
              LEFT JOIN rating AS r ON p.id = r.productId AND r.userId = :userId
              WHERE o.orderId = :orderId
              GROUP BY o.orderId";
        $stmt = $this->bdd->prepare($query);
        $stmt->bindParam(':orderId', $orderId);
        $stmt->bindParam(':userId', $userId);
        $stmt->execute();
        $order = $stmt->fetch(\PDO::FETCH_ASSOC);

        $produits = '[' . $order['produits'] . ']';
        $formattedOrder = [
            'idCommande' => $order['orderId'],
            'dateCommande' => $order['date'],
            'prixTotalCommande' => $order['prixTotal'],
            'produits' => json_decode($produits, true)
        ];

        return $formattedOrder;
    }

    public function getOrderSellerById($userId)
    {
        $query = "
            SELECT o.orderId, o.userId, o.date, o.prixTotal, op.quantity, op.productId
            FROM orders AS o
            JOIN orders_product AS op ON o.orderId = op.orderId
            JOIN product AS p ON op.productId = p.id
            WHERE o.orderId IN (
                SELECT o.orderId
                FROM orders AS o
                JOIN orders_product AS op ON o.orderId = op.orderId
                JOIN product AS p ON op.productId = p.id
                WHERE p.userId = :userId
            )
        ";

        $stmt = $this->bdd->prepare($query);
        $stmt->bindParam(':userId', $userId);
        $stmt->execute();

        $orders = array();

        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $order = array(
                'orderId' => $row['orderId'],
                'date' => $row['date'],
                'userId' => $row['userId'],
                'produits' => array(
                    'quantity' => $row['quantity'],
                    'productId' => $row['productId']
                )
            );
            $orders[] = $order;
        }

        return $orders;
    }

    public function addProductToCommand($orderId, $productId, $quantity)
    {
        $query = "INSERT INTO orders_product(orderId, productId, quantity) VALUES (:orderId, :productId, :quantity)";

        $stmt = $this->bdd->prepare($query);
        $stmt->bindParam(':orderId', $orderId);
        $stmt->bindParam(':productId', $productId);
        $stmt->bindParam(':quantity', $quantity);
        $stmt->execute();
        $stmt->fetch(\PDO::FETCH_ASSOC);
    }
}
