<?php

namespace Model;

class ProductManager extends ModelManager
{
    public function __construct()
    {
        parent::__construct("product");
    }

    public function getAllWithRating()
    {
        $req = $this->bdd->prepare("SELECT product.*, AVG(rating.value) AS averageRating 
                                    FROM product 
                                    LEFT JOIN rating ON rating.productId = product.id 
                                    WHERE visibility = 1
                                    GROUP BY product.id");
        $req->execute();
        $req->setFetchMode(\PDO::FETCH_OBJ);
        $productList = $req->fetchAll();
        $req = $this->bdd->prepare("SELECT AVG(rating.value) as sellerRating FROM product
                                    INNER JOIN rating
                                    ON rating.productId = product.id
                                    WHERE product.userId = :user_id
                                    GROUP BY product.userId");
        foreach ($productList as $product) {
            $req->bindParam(":user_id", $product->userId);
            $req->execute();
            $req->setFetchMode(\PDO::FETCH_OBJ);
            $rating = $req->fetch();
            if (isset($rating->sellerRating)) {
                $product->sellerRating = $rating->sellerRating;
            } else {
                $product->sellerRating = 0;
            }
        }
        return $productList;
    }

    public function getAllWithRatingFilter($merchantId)
    {
        $req = $this->bdd->prepare("SELECT product.*, AVG(rating.value) AS averageRating 
                                    FROM product 
                                    LEFT JOIN rating ON rating.productId = product.id
                                    WHERE product.userId = :merchant_id
                                    GROUP BY product.id");
        $req->bindParam(":merchant_id", $merchantId);
        $req->execute();
        $req->setFetchMode(\PDO::FETCH_OBJ);
        $productList = $req->fetchAll();

        $req = $this->bdd->prepare("SELECT AVG(rating.value) as sellerRating FROM product
                                    INNER JOIN rating
                                    ON rating.productId = product.id
                                    WHERE product.userId = :user_id
                                    GROUP BY product.userId");

        foreach ($productList as $product) {
            $req->bindParam(":user_id", $product->userId);
            $req->execute();
            $req->setFetchMode(\PDO::FETCH_OBJ);
            $rating = $req->fetch();
            if (isset($rating->sellerRating)) {
                $product->sellerRating = $rating->sellerRating;
            } else {
                $product->sellerRating = 0;
            }
        }

        return $productList;
    }

    public function getByIdWithRating($product_id)
    {
        $req = $this->bdd->prepare("SELECT product.*, AVG(rating.value) AS averageRating 
                                    FROM product 
                                    LEFT JOIN rating ON rating.productId = product.id 
                                     WHERE product.id = :product_id");
        $req->bindParam(":product_id", $product_id);
        $req->execute();
        $req->setFetchMode(\PDO::FETCH_OBJ);
        $product = $req->fetch();
        if ($product) {
            $req = $this->bdd->prepare("SELECT AVG(rating.value) as sellerRating FROM product
                                        INNER JOIN rating
                                        ON rating.productId = product.id
                                        WHERE product.userId = :user_id
                                        GROUP BY product.userId");
            $req->bindParam(":user_id", $product->userId);
            $req->execute();
            $req->setFetchMode(\PDO::FETCH_OBJ);
            $rating = $req->fetch();
            if (isset($rating->sellerRating)) {
                $product->sellerRating = $rating->sellerRating;
            } else {
                $product->sellerRating = 0;
            }
        }
        return $product;
    }

    public function toggleVisibility($product_id)
    {
        $req = $this->bdd->prepare("UPDATE product SET visibility = !visibility WHERE id = :product_id");
        $req->bindParam(":product_id", $product_id);
        $req->execute();
        return $req->rowCount() == 1;
    }

    public function searchByName($search)
    {
        $req = $this->bdd->prepare("SELECT product.*, AVG(rating.value) AS averageRating 
                                    FROM product 
                                    LEFT JOIN rating ON rating.productId = product.id 
                                    WHERE product.nom LIKE :search
                                    AND visibility = 1
                                    GROUP BY product.id");
        $req->bindValue(":search", "%$search%");
        $req->execute();
        $req->setFetchMode(\PDO::FETCH_OBJ);
        $productList = $req->fetchAll();
        $req = $this->bdd->prepare("SELECT AVG(rating.value) as sellerRating FROM product
                                    INNER JOIN rating
                                    ON rating.productId = product.id
                                    WHERE product.userId = :user_id
                                    GROUP BY product.userId");
        foreach ($productList as $product) {
            $req->bindParam(":user_id", $product->userId);
            $req->execute();
            $req->setFetchMode(\PDO::FETCH_OBJ);
            $rating = $req->fetch();
            if (isset($rating->sellerRating)) {
                $product->sellerRating = $rating->sellerRating;
            } else {
                $product->sellerRating = 0;
            }
        }
        return $productList;
    }

    public function MerchantSearchByName($merchantId, $search)
    {
        $req = $this->bdd->prepare("SELECT product.*, AVG(rating.value) AS averageRating 
                                    FROM product 
                                    LEFT JOIN rating ON rating.productId = product.id 
                                    WHERE product.nom LIKE :search
                                    AND product.userId = :user_id
                                    GROUP BY product.id");
        $req->bindValue(":search", "%$search%");
        $req->bindValue(":user_id", $merchantId);
        $req->execute();
        $req->setFetchMode(\PDO::FETCH_OBJ);
        $productList = $req->fetchAll();
        $req = $this->bdd->prepare("SELECT AVG(rating.value) as sellerRating FROM product
                                    INNER JOIN rating
                                    ON rating.productId = product.id
                                    WHERE product.userId = :user_id
                                    GROUP BY product.userId");
        foreach ($productList as $product) {
            $req->bindParam(":user_id", $product->userId);
            $req->execute();
            $req->setFetchMode(\PDO::FETCH_OBJ);
            $rating = $req->fetch();
            if (isset($rating->sellerRating)) {
                $product->sellerRating = $rating->sellerRating;
            } else {
                $product->sellerRating = 0;
            }
        }
        return $productList;
    }
    public function RateProductById($productId, $userId, $rate)
    {
        $query = $this->bdd->prepare("SELECT * FROM rating WHERE productId = :productId AND userId = :userId");
        $query->bindParam(':productId', $productId);
        $query->bindParam(':userId', $userId);
        $query->execute();

        if ($query->rowCount() > 0) {
            $updateQuery = $this->bdd->prepare("UPDATE rating SET value = :rate WHERE productId = :productId AND userId = :userId");
            $updateQuery->bindParam(':rate', $rate);
            $updateQuery->bindParam(':productId', $productId);
            $updateQuery->bindParam(':userId', $userId);
            $updateQuery->execute();
            return true;
        } else {
            $insertQuery = $this->bdd->prepare("INSERT INTO rating (productId, userId, value) VALUES (:productId, :userId, :rate)");
            $insertQuery->bindParam(':productId', $productId);
            $insertQuery->bindParam(':userId', $userId);
            $insertQuery->bindParam(':rate', $rate);
            $insertQuery->execute();
            return true;
        }
        return false;
    }
}
