<?php

namespace Controller;

class OrderController extends BaseController
{
    public function getOrdersByUserId($id)
    {
        $orders = $this->orderManager->getUserOrders($id);
        if ($orders) {
            $this->JSON($orders);
        } else {
            echo 'Produit non trouvé';
        }
    }

    public function getOrderById($orderId, $userId)
    {
        $order = $this->orderManager->getOrderById($orderId, $userId);
        if ($order) {
            $this->JSON($order);
        } else {
            echo 'Commande non trouvée';
        }
    }

    public function getAllOrdersSeller($userId)
    {
        $res = $this->orderManager->getOrderSellerById($userId);

        $orders = array();

        foreach ($res as $item) {
            $orderId = $item['orderId'];

            if ($item['userId'] == $userId) {
                $product = $this->productManager->getByIdWithRating($item['produits']['productId']);
                $item['produits']['product'] = $product;

                // Si la commande existe déjà dans le tableau des commandes, ajouter le produit à cette commande
                if (isset($orders[$orderId])) {
                    $orders[$orderId]['produits'][] = $item['produits'];
                }
                // Sinon, créer une nouvelle entrée pour cette commande
                else {
                    $order = array(
                        'orderId' => $orderId,
                        'date' => $item['date'],
                        'userId' => $item['userId'],
                        'produits' => array($item['produits'])
                    );
                    $orders[$orderId] = $order;
                }
            }
        }

        // Convertir le tableau des commandes en tableau indexé
        $orders = array_values($orders);

        $this->JSON($orders);
    }

    public function CreateOrder($userId, $products)
    {
        $listProducts = json_decode($products, true);
        $date = date("Y-m-d");
        $prixTotal = 0;

        $user = $this->userManager->getById($userId);

        $orderId = "" . strtoupper(substr($user->username, 0, 3)) . date("ymdHi");

        $qttProducts = [];

        foreach ($listProducts as $productId) {
            $p = $this->productManager->getById($productId);
            if ($p) {
                $price = $p->prix;
                $prixTotal += $price;
                if (isset($qttProducts[$productId])) {
                    $qttProducts[$productId]++;
                } else {
                    $qttProducts[$productId] = 1;
                }
            }
        }

        $order = new \stdClass();
        $order->userId = $userId;
        $order->orderId = $orderId;
        $order->date = $date;
        $order->prixTotal = $prixTotal;

        $this->orderManager->create($order);

        foreach ($qttProducts as $productId => $quantity) {
            $orderProduct = new \stdClass();
            $orderProduct->orderId = $orderId;
            $orderProduct->productId = $productId;
            $orderProduct->quantity = $quantity;

            $this->orderManager->addProductToCommand($orderId, $productId, $quantity);
        }
    }

    function getOrderSellerById($orderId, $userId)
    {
        $order = $this->orderManager->getOrderById($orderId);

        if ($order) {
            unset($order['prixTotalCommande']);
            $order['produits'] = array_filter($order['produits'], function ($produit) use ($userId) {
                return $produit['userId'] == $userId;
            });

            $this->JSON($order);
        } else {
            echo 'Commande non trouvée';
        }
    }
}
