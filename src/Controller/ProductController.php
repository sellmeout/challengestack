<?php

namespace Controller;

class ProductController extends BaseController
{
    public function ShowProductList()
    {
        $productList = $this->productManager->getAllWithRating();
        if ($productList) {
            $this->JSON($productList);
        } else {
            echo 'Aucun produit';
        }
    }

    public function ShowProductListMerchant($merchantId)
    {
        $productList = $this->productManager->getAllWithRatingFilter($merchantId);
        if ($productList) {
            $this->JSON($productList);
        } else {
            echo 'Aucun produit';
        }
    }

    public function GetProductById($id)
    {
        $product = $this->productManager->getByIdWithRating($id);
        if ($product) {
            $this->JSON($product);
        } else {
            echo 'Produit non trouvé';
        }
    }

    public function CreateProduct($nom, $description, $prix, $userId, $visibility)
    {
        $image =    "shoe-" . rand(1, 5) . ".png";

        $product = new \stdClass();
        $product->nom = $nom;
        $product->description = $description;
        $product->prix = $prix;
        $product->image = $image;
        $product->userId = $userId;
        $product->visibility = $visibility;

        $this->productManager->create($product);
    }


    public function ToggleVisibility($id)
    {
        $this->productManager->toggleVisibility($id);
    }

    public function searchByName($name)
    {
        $productList = $this->productManager->searchByName($name);
        if ($productList) {
            $this->JSON($productList);
        } else {
            echo 'Aucun produit';
        }
    }

    public function MerchantSearchByName($idMerchant, $name)
    {
        $productList = $this->productManager->MerchantSearchByName($idMerchant, $name);
        if ($productList) {
            $this->JSON($productList);
        } else {
            echo 'Aucun produit';
        }
    }

    public function RateProduct($productId, $rate, $userId)
    {
        if ($this->productManager->RateProductById($productId, $userId, $rate)) {
            return $rate;
        } else {
            echo "Error";
        }
    }
}
