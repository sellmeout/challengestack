<?php

namespace Controller;


class UserController extends BaseController
{
    function SignUp($mail, $password, $username, $role)
    {
        $validRoles = ["seller", "customer"];

        if (!in_array($role, $validRoles)) {
            echo "Invalid role";
            return;
        }

        $user = new \stdClass();
        $user->mail = $mail;
        $user->password = password_hash($password, PASSWORD_DEFAULT);
        $user->username = $username;
        $user->role = $role;

        if ($this->userManager->create($user)) {
            unset($user->password);

            $this->JSON($user);
        } else {
            echo "Error";
        }
    }

    function SignIn($mail, $password)
    {
        $user = $this->userManager->getByEmail($mail);
        if ($user) {
            if (password_verify($password, $user->password)) {
                unset($user->password);
                $_SESSION['user'] = $user;

                $this->JSON($user);
            } else {
                echo "Mot de passe erroné";
            }
        } else {
            echo "Email incorrect";
        }
    }

    function GetUserById($id)
    {
        $user = $this->userManager->getById($id);
        if ($user) {
            unset($user->password);
            $this->JSON($user);
        } else {
            echo 'Utilisateur non trouvé';
        }
    }
}
