<?php

namespace Controller;

class BaseController
{

    protected $route;
    private $params;
    protected $userManager;
    protected $productManager;
    protected $orderManager;

    public function __construct($route)
    {
        $this->route = $route;
        $this->params = array();
        $this->loadManager();
    }

    public function loadManager()
    {
        if (!empty($this->route->manager)) {
            foreach ($this->route->manager as $manager) {
                $managerName = $manager . "Manager";
                $managerClass = "Model\\" . $managerName;
                $this->{lcfirst($managerName)} = new $managerClass();
            }
        }
    }

    public function JSON($data)
    {
        header("content-type: application/json");
        echo json_encode($data);
    }

    public function addParam($key, $value)
    {
        $this->params[$key] = $value;
    }
}
